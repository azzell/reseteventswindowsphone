﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Facebook;
using Newtonsoft.Json;
using ResetWPF.ViewModel;
namespace ResetWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Result latest = new Result();
        Result next = new Result();
        Result today = new Result();
        const string AppID = "1098276163543902";
        const string Secret = "2ded7d525c97f30283e8c6a36dd6ccd8";
        dynamic ac;
        double bth = 425;
        double btw = 625;
        string odp;
        dynamic result2;
        public MainWindow()
        {
            InitializeComponent();
            var fbClient = new FacebookClient();
            dynamic result = fbClient.Get("oauth/access_token", new
            {
                client_id = AppID,
                client_secret = Secret,
                grant_type = "client_credentials"
            });
            ac = result;
            fbClient.AccessToken = result.access_token;
            ac = result.access_token;
            string me = Convert.ToString(fbClient.Get("/ResetATH/events?fields=name,description,start_time"));
            Result js = JsonConvert.DeserializeObject<Result>(me);

            foreach (var item in js.data)
            {
                DateTime dzis = DateTime.Now;
                if (dzis.CompareTo(item.start_time) > 0)
                {
                    latest.data.Add(item);
                }
                else if (dzis.CompareTo(item.start_time) < 0)
                {
                    next.data.Add(item);
                }
                else
                {
                    today.data.Add(item);
                }

            }

            dataGrid.DataContext = latest;

        }
        //private async void get()
        //{
        //    try
        //    {
        //        var fb = new FacebookClient();
        //        fb.AccessToken = ac.access_token;
        //        result2 = await fb.GetTaskAsync("/ResetATH/events?fields=name,description,start_time");
        //        int one=11;
        //    }
        //    catch (FacebookApiException ex)
        //    {
        //        odp = Convert.ToString(ex);

        //        throw;
        //    }
        //}
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox.SelectedIndex == 0)
            {
                dataGrid.DataContext = latest;
            }
            else if (comboBox.SelectedIndex == 1)
            {
                dataGrid.DataContext = next;
            }
            else
            {
                dataGrid.DataContext = today;
            }
            
        }

        private void window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //comboBox.Height = comboBox.ActualHeight / bth * window.ActualHeight;
            //comboBox.Width = comboBox.ActualWidth / btw * window.ActualWidth;
            dataGrid.Height = dataGrid.ActualHeight / bth * window.ActualHeight;
            dataGrid.Width = dataGrid.ActualWidth / btw * window.ActualWidth;
            btw = window.ActualWidth;
            bth = window.ActualHeight;
        }
    }
}
