﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResetWPF.ViewModel
{
    class Data
    {
        public string name { get; set; }
        public string description { get; set; }
        public DateTime start_time { get; set; }
    }
}
