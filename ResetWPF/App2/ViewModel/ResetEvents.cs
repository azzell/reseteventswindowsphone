﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facebook;
using Newtonsoft.Json;
using System.Threading;
namespace App1.ViewModel
{
    class ResetEvents
    {
        public FacebookClient Client { get; set; }
        public Result REvents { get; set; } = new Result();
        public Result REvents2 { get; set; } = new Result();
        public Result Past { get; set; } = new Result();
        public Result SPast { get; set; } = new Result();
        public Result Next { get; set; } = new Result();
        public Result SNext { get; set; } = new Result();

        const string AppID = "Your app id";
        const string Secret = "Your app secret";
        
        public  ResetEvents()
        {
           
            Client = new FacebookClient();
            Geting();
            
        }
        private async void Geting()
        {
            try
            {
                dynamic result = await Client.GetTaskAsync("oauth/access_token", new
                {
                    client_id = AppID,
                    client_secret = Secret,
                    grant_type = "client_credentials"
                });
                Client.AccessToken = result.access_token;
                dynamic result2 = await Client.GetTaskAsync("/ResetATH/events?fields=id,name,description,start_time");
                string tmp = Convert.ToString(result2);
                REvents = JsonConvert.DeserializeObject<Result>(tmp);
                REvents2 = JsonConvert.DeserializeObject<Result>(tmp);
                Sort();
                Cut();
                DateChange();

            }
            catch (FacebookApiException ex)
            {
                string em = Convert.ToString(ex);
                throw;
            }
        }
        private void Sort()
        {
            foreach (var item in REvents.data)
            {
                DateTime today = DateTime.Now;
                if (today.CompareTo(item.start_time) > 0)
                {
                    //Past.data.Add(item);
                    SPast.data.Add(item);
                }
                else if (today.CompareTo(item.start_time) <= 0)
                {
                    //Next.data.Add(item);
                    SNext.data.Add(item);
                }
            }
        }
        private void Cut()
        {
            for (int i = 0; i < SPast.data.Count; i++)
            {
                if (SPast.data[i].description.Length>150)
                {
                SPast.data[i].description = SPast.data[i].description.Remove(150).Insert(150,"...");

                }
                else
                {
                    SPast.data[i].description = SPast.data[i].description.Insert(SPast.data[i].description.Length, "...");
                }
            }
            for (int i = 0; i < SNext.data.Count; i++)
            {
                if (SNext.data[i].description.Length > 150)
                {
                    SNext.data[i].description = SNext.data[i].description.Remove(150).Insert(150, "...");

                }
                else
                {
                    SNext.data[i].description = SNext.data[i].description.Insert(SNext.data[i].description.Length, "...");
                }
                

            }
        }
        private void DateChange()
        {
            foreach (var item in REvents2.data)
            {
                DateTime tmp = item.start_time;
                item.start_time = tmp.ToLocalTime(); 
            }
        }    
        //private async void Rest()
        //{
        //    Client = new FacebookClient();
        //    await Geting();
        //}
    }
}
